import { BotFactory } from '../src/Types.d.ts'

const bot: BotFactory = () => {
	const HistIndex = {
		myMove: 0,
		theirMove: 1,
		result: 2
	};

	const MoveIndex = {
		0: 'rock',
		1: 'paper',
		2: 'scissors'
	};

	let history = Array();

	let lossCounter = 0;
	let currentStrat = 1;

	function goNextStrat() {
		if (currentStrat == 3) {
			currentStrat = 1;
		}
		else {
			currentStrat++;
		}
	}

	function getNextMove(prevMove: string, offset: int = 1) {
		return MoveIndex[(Object.keys(MoveIndex).indexOf(prevMove) + 1 + offset) % 3];
	}

	// Called after each round with the results
	// result is 1 for win, -1 for loss, 0 for tie
	const Report = (myMove: string, theirMove: string, result: int) => {
		if (result == -1) {
			lossCounter++;
			if (lossCounter == 2) {
				lossCounter = 0;
				goNextStrat();
			}

		}

		history.push([myMove, theirMove, result]);
	}

	const Shoot = () => {
		// console.log(currentStrat);
		// Decide what to do
		if (history.length == 0) {
			return 'scissors';
		}

		if (currentStrat == 1) {	//same move again and again
			if (history[history.length - 1][HistIndex.result] == -1) {
				return getNextMove(history[history.length - 1][HistIndex.myMove], 2);
			}
			else {
				return getNextMove(history[history.length - 1][HistIndex.myMove], 0);
			}
		}
		else if (currentStrat == 2) {	//throw what opponent threw 2 turns ago, iterated by 1
			if (history.length == 1) {
				return 'rock';
			}
			return getNextMove(history[history.length - 2][HistIndex.theirMove]);
		}
		else {  // if (currentStrat == 3) {
			if (history.length % 2 == 1) {	//on odd histories, iterate by 2 based on mymove
				return getNextMove(history[history.length - 1][HistIndex.myMove], 2);
			}
			else {	//on even history count, stay the same
				return getNextMove(history[history.length - 1][HistIndex.myMove], 0);
			}
		}
	}

	return {
		Name: 'Scizorzz!',
		Shoot,
		Report,
	}
}

export default bot
