import Bot from './bots/defensiveBot.ts'
import Cycler from './bots/example/Cycler.ts'
import { battle } from './src/RockPaperScissors.ts'

battle(Bot(), Cycler(), 10)
